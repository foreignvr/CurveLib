﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves.Demo
{
	/// <summary>
	/// DemoInstructions. Alters the timescale by keypress for demo purposes
	/// author: jacob
	/// </summary>
	public class DemoInstructions : MonoBehaviour
	{
		#region Monobehaviour

		void Start ()
		{
			Screen.SetResolution (600, 960, false);
		}

		void Update ()
		{
			if (Input.GetKeyDown (KeyCode.A))
				Time.timeScale = 1f;
			if (Input.GetKeyDown (KeyCode.S))
				Time.timeScale = 0.5f;
			if (Input.GetKeyDown (KeyCode.D))
				Time.timeScale = 0.1f;
			if (Input.GetKeyDown (KeyCode.F))
				Time.timeScale = 2f;
		}

		#endregion
	}
}