﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves.Demo
{
	/// <summary>
	/// DemoWaterfallRandomSpawner. Spawns 3 prefab types repeatedly onscreen to show sync settings demo.
	/// </summary>
	public class DemoWaterfallRandomSpawner : MonoBehaviour
	{
		#region Inspector Scene Children

		[Header ("Scene Objects")]
		[SerializeField]
		Transform[] spawnAnchors;
		[SerializeField]
		GameObject[] spawnableObjects;

		#endregion

		#region Private Variables

		const int numobjects = 2;
		const float lifetimeoverthree = (5.1f / (float)numobjects);

		float nextSpawnTime = 1f;
		int mod = 0;

		#endregion

		#region Monobehaviour

		void Update ()
		{
			nextSpawnTime -= Time.deltaTime;
			if (nextSpawnTime < 0) {
				for (var count = 0; count < spawnableObjects.Length; count++) {
					var spawned = Instantiate (spawnableObjects [count], spawnAnchors [count]);
					spawned.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, (mod * 100f) - 50f);
				}
				mod = (mod + 1) % numobjects;
				nextSpawnTime = lifetimeoverthree + nextSpawnTime;
			}
		}

		#endregion
	}
}