﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves.Demo
{
	/// <summary>
	/// Simple self destruct script based on inspector lifetime config.
	/// </summary>
	public class DemoWaterfallObject : MonoBehaviour
	{
		#region Inspector Config

		[Header ("Config")]
		[SerializeField]
		float lifetime = 5f;

		#endregion

		#region Monobehaviour

		void Update ()
		{
			lifetime -= Time.deltaTime;
			if (lifetime <= 0f) {
				enabled = false;
				Destroy (this.gameObject);
			}
		}

		#endregion
	}
}