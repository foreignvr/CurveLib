﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Curves.Demo
{
	/// <summary>
	/// Simple UI hooks for the demo user. Activates objects or triggers curve based on what's linked in inspector.
	/// </summary>
	public class DemoHover : MonoBehaviour
, IPointerEnterHandler
, IPointerExitHandler
	{
		#region Inspector Scene Children

		[Header ("Scene objects (Optional)")]
		[SerializeField]
		Curve cover;
		[SerializeField]
		Curve cout;
		[SerializeField]
		GameObject activateMe;

		#endregion

		#region IPointerEnterHandler

		public void OnPointerEnter (PointerEventData e)
		{
			if (cover != null)
				cover.Trigger ();
			if (activateMe != null)
				activateMe.SetActive (true);
		}

		#endregion

		#region IPointerExitHandler

		public void OnPointerExit (PointerEventData e)
		{
			if (cover != null)
				cover.Stop ();
			if (cout != null)
				cout.Trigger ();
			if (activateMe != null)
				activateMe.SetActive (false);
			// if cover is playing
			// check if cover.fnishlisteners == 0
			// if so: add this triggerme func as a listener func closer.
		}

		#endregion
	}
}