﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves
{
	public class CurveShake : Curve
	{
		#region Inspector Additions

		[Header ("CurveShake")]
		public float intensity;
		public float intensity2;
		public float intensity3;

		#endregion

		#region Curve

		protected override void Step (float t)
		{
			if (Mathf.Approximately (t, 1f) || Mathf.Approximately (t, 0f)) {
				transform.localPosition = Vector3.zero;
				transform.localRotation = Quaternion.identity;
				//transform.localScale = Vector3.one;
			} else {
				transform.localPosition = new Vector3 (Random.value * intensity, Random.value * intensity, 0f);
				transform.localRotation = Quaternion.Euler (0f, 0f, Random.value * intensity2);
				/*transform.localScale = new Vector3 (0.75f + (Random.value * intensity3),
				0.75f + (Random.value * intensity3), 0.75f + (Random.value * intensity3));*/
			}
		}

		#endregion
	}
}