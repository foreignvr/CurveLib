﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves
{
	public class CurveColor : Curve
	{
		#region Inspector Additions

		[Header ("CurveColor")]
		public Color start;
		public Color end;

		#endregion

		#region Private Members

		UnityEngine.UI.Image cacheImage;
		UnityEngine.UI.Text cacheText;

		bool isText;
		bool initialized = false;

		#endregion

		#region Monobehaviour

		/// <summary>
		/// internally cache Img || Text to simplify user process.
		///  finished by calling base.OnEnable (); for inherited Curve
		/// </summary>
		protected override void OnEnable ()
		{
			if (initialized) {
				base.OnEnable ();
				return;
			}

			cacheImage = GetComponent<UnityEngine.UI.Image> ();
			if (cacheImage == null) {
				isText = true;
				cacheText = GetComponent<UnityEngine.UI.Text> ();
				if (cacheText == null) {
					Debug.LogError ("Failure to cache CurveColor " + name);
				}
			}
			initialized = true;
			base.OnEnable ();
		}

		#endregion

		#region Curve


		protected override void Step (float t)
		{
			if (!initialized)
				return;
		
			var vec = start;//isReverse ? end : start;
			var vec2 = end;//isReverse ? start : end;
			if (isText)
				cacheText.color = Color.LerpUnclamped (vec, vec2, t);
			else
				cacheImage.color = Color.LerpUnclamped (vec, vec2, t);
		}

		#endregion
	}
}