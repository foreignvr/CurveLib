﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Utils;

namespace Curves
{
	public class Curve : MonoBehaviour
	{
		#region Inspector Config

		[Header ("Required")]
		public CurveCatalog catalog;

		[Tooltip ("String reference to a CurveCatalog item's productid.")]
		[SerializeField]
		string curveid;

		[Tooltip ("Duration in secs of the animation cycle not including delays. This is paired with the curve directly to drive steps.")]
		[SerializeField]
		float duration;

		[Header ("Optional")]
		[Tooltip ("Enabling this will cause the curve to run every time the game object is enabled.")]
		[SerializeField]
		bool onEnable;


		[Tooltip ("Delay in secs from the execution to the first animation cycle.")]
		[SerializeField]
		float initialDelay;

		[Tooltip ("Enabling this will cause the animation to loop after completion.")]
		//needs to be public for the inspector
		[SerializeField]
		bool loop;

		[Tooltip ("Delay in secs between each animation cycle not including the first execution (initialDelay).")]
		[SerializeField]
		float loopedDelay;

		[Tooltip ("Enabling this will randomize the starting stride in the animation cycle.")]
		[SerializeField]
		bool randomizeStartTime;

		[Tooltip ("Enabling this will synchronize the start time by duration allowing different activation time curves to align.")]
		[SerializeField]
		bool syncStartTime;

		#endregion

		#region Private Members

		float randomizedOffsetTime = -1f;
		float startTime;
		float completeTime;
		bool isRunning;
		AnimationCurve animCurve;
		protected bool isReverse;

		bool _isPlaying;
		bool stopLooping;

		Coroutine outstandingLoopRun;

		#endregion

		#region Public Members

		public bool isLoopEnabled {
			get { return loop; }
		}

		public bool isPlaying {
			get { return _isPlaying; }
		}

		#endregion

		#region MonoBehaviour

		//Inherited in CurveColor to cache data
		protected virtual void OnEnable ()
		{
			GetCurvePointer ();
			if (onEnable) {
				Trigger ();
			}
		}

		void OnDisable ()
		{
			if (isRunning)
				Stop (true);
		}

		void Start ()
		{
			if (Mathf.Approximately (duration, 0f)) {
				//Common new object issue. Forgot to set duration.
				Debug.LogError ("Curve duration == 0f: " + this.name);
			}
		}

		void Update ()
		{
			if (isRunning) {
				if (Time.time < completeTime) {
					CalculatedStep ();
				} else {
					if (loop && !stopLooping) {
						EndStep ();
						Loop ();
					} else {
						Halt ();
					}
				}
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Delayed execution when we OnEnable before the Catalog is able to initialize.
		/// </summary>
		/// <returns>WaitUntil Catalog is ready.</returns>
		IEnumerator ExecuteOnEnableTrigger ()
		{
			yield return new WaitUntil (() => true/* CurveCatalog.ready*/);

			Trigger ();
		}

		/// <summary>
		/// Calculated Step is the routine for using "real" time data to drive Step calls.
		/// </summary>
		void CalculatedStep ()
		{
			var t = (Time.time - startTime) / duration;
			if (isReverse)
				t = 1f - t;

			//account for overflow caused by syncStartTime & randomizeStartTime
			if (t > 1f)
				t -= (int)t;
			Step (animCurve.Evaluate (t));
		}

		void EndStep ()
		{
			var t = (completeTime - startTime) / duration;
			if (isReverse)
				t = 1f - t;
			//account for overflow caused by syncStartTime & randomizeStartTime
			if (t > 1f && (syncStartTime || randomizeStartTime)) {
				t -= (int)t;
			}
			Step (animCurve.Evaluate (t));
		}

		/// <summary>
		/// Final logical step in "stopping" the animation. This happens every circumstance that
		///  the Curve Run concludes except if loop is enabled. (Timeout or user Stop)
		/// </summary>
		void Halt ()
		{
			EndStep ();
			//Step (animCurve.Evaluate (isReverse ? 0f : 1f));
			isRunning = false;
			isReverse = false;
			_isPlaying = false;
		}

		IEnumerator delayedIsRunning (float delay)
		{
			yield return new WaitForSeconds (delay);

			isRunning = true;
			RecalculateStartCompleteTimes ();
		}

		void GetCurvePointer ()
		{
			if (catalog == null)
				Debug.LogError ("Missing curve catalog pointer in: " + name);
			var item = catalog.GetItem (curveid);
			if (item == null) {
				Debug.LogError ("No catalog item found for: " + name);
				return;
			}
			animCurve = item.Curve;
		}

		void RecalculateStartCompleteTimes ()
		{
			if (syncStartTime) {
				startTime = (int)(Time.time / duration) * duration;
			} else if (randomizeStartTime) {
				if (Mathf.Approximately (randomizedOffsetTime, -1f))
					randomizedOffsetTime = Random.value;
				startTime = Time.time - (duration * randomizedOffsetTime);
			} else {
				startTime = Time.time;
			}
			completeTime = Time.time + duration;
		}

		void Loop ()
		{
			Run (loopedDelay);
		}

		void Run (float delay)
		{
			//isRunning will be used by Update to drive calculatedSteps on the next frame and on.
			isRunning = Mathf.Approximately (delay, 0f);

			//if there was a delay requested, we'll setup a coroutine to set isRunning at that timeout.
			if (!isRunning)
				outstandingLoopRun = StartCoroutine (delayedIsRunning (delay));
			//either way, we want to use this opportunity to establish starttime & completetime values.
			// this is due to usage in resets.
			RecalculateStartCompleteTimes ();
		}

		/// <summary>
		/// Steps the specified time. Used to drive animation progression & resets.
		/// </summary>
		/// <param name="t">T.</param>
		protected virtual void Step (float t)
		{
		}

		#endregion

		#region Public Functions

		public void Trigger (bool reverse = false)
		{
			_isPlaying = true;

			if (animCurve == null) {
				Debug.LogError ("Attempted to Trigger without a valid animCurve: " + name);
				return;
			}
			
			stopLooping = false;
			isReverse = reverse;
			Run (initialDelay);
			CalculatedStep ();
		}

		public void Stop (bool immediate = false)
		{
			_isPlaying = false;
			stopLooping = true;

			if (outstandingLoopRun != null)
				StopCoroutine (outstandingLoopRun);
			
			if (immediate)
				Halt ();
		}

		#endregion
	}
}