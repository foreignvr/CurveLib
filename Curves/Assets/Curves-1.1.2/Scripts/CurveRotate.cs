﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves
{
	public class CurveRotate : Curve
	{
		#region Inspector Additions

		[Header ("CurveRotate")]
		public Vector3 start;
		public Vector3 end;

		#endregion

		#region Curve

		protected override void Step (float t)
		{
			transform.localRotation = Quaternion.Euler (Vector3.LerpUnclamped (start, end, t));
		}

		#endregion
	}
}