﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves
{
	public class CurveScale : Curve
	{
		#region Inspector Additions

		[Header ("CurveScale")]
		public Vector3 startPosition;
		public Vector3 endPosition;

		#endregion

		#region Curve

		protected override void Step (float t)
		{
			//var vec = isReverse ? endPosition : startPosition;
			//var vec2 = isReverse ? startPosition : endPosition;

			var vec = startPosition;//isReverse ? end : start;
			var vec2 = endPosition;//isReverse ? start : end;
			transform.localScale = Vector3.LerpUnclamped (vec, vec2, t);
		}

		#endregion

		#region Public Functions

		public void ReconfigurePositions (Vector3 start, Vector3 end)
		{
			startPosition = start;
			endPosition = end;
		}

		#endregion
	}
}