﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves
{
	public class CurveMove : Curve
	{
		#region Inspector Additions

		[Header ("CurveMove")]
		public Vector3 startPosition;
		public Vector3 endPosition;

		#endregion

		#region Curve

		protected override void Step (float t)
		{
			transform.localPosition = Vector3.LerpUnclamped (startPosition, endPosition, t);
		}

		#endregion

		#region Public Functions

		public void ReconfigurePositions (Vector3 start, Vector3 end)
		{
			startPosition = start;
			endPosition = end;
		}

		#endregion
	}
}