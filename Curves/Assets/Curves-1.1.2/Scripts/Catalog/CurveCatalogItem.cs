﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves
{
	[System.Serializable]
	public class CurveCatalogItem : Utils.CatalogSOItem
	{
		public AnimationCurve Curve;
	}
}