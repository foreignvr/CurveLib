﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Curves
{
	/// <summary>
	/// Curve catalog instantiates the abstract CatalogSO with CurveCatalogItem as it's template item. This allows for
	/// the generation of a list of curves. See "DemoCurves"
	/// </summary>
	[CreateAssetMenu (fileName = "CurveCatalog", menuName = "Curves-1.1.0/CurveCatalog", order = 1)]
	public class CurveCatalog : Utils.CatalogSO<CurveCatalogItem>
	{
	}
}