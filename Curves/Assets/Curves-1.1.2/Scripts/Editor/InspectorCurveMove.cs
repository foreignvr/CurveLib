﻿#if UNITY_EDITOR

namespace Curves
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEditor;

	/// <summary>
	/// Inspector extension of CurveMove. Adds buttons to set position for easier editing.
	/// </summary>
	[CustomEditor (typeof(CurveMove))]
	[CanEditMultipleObjects]
	public class InspectorCurveMove : Editor
	{
		#region Editor

		public override void OnInspectorGUI ()
		{
			DrawDefaultInspector ();

			var mycurve = (CurveMove)target;
			if (GUILayout.Button ("Start position")) {
				mycurve.transform.localPosition = mycurve.startPosition;
			} else if (GUILayout.Button ("End position")) {
				mycurve.transform.localPosition = mycurve.endPosition;
			}
		}

		#endregion
	}
}
#endif