﻿#if UNITY_EDITOR

namespace Curves
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEditor;

	[CustomEditor (typeof(CurveCatalog), true)]
	public class InspectorCurveCatalog : Editor
	{
		#region Editor

		public override void OnInspectorGUI ()
		{
			DrawDefaultInspector ();
			var mytarget = (CurveCatalog)target;
			if (GUILayout.Button ("Cache")) {
				mytarget.ProcessItemsToCache ();
			}
		}

		#endregion
	}
}
#endif
