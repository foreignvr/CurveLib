﻿#if UNITY_EDITOR
namespace Curves
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEditor;

	/// <summary>
	/// Inspector extension of Curve. .
	/// </summary>
	[CustomEditor (typeof(Curve), true)]
	[CanEditMultipleObjects]
	public class InspectorCurve : Editor
	{
		#region Editor

		public override void OnInspectorGUI ()
		{
			//DrawDefaultInspector ();
			var mytarget = (Curve)target;

			//Sanity check

			if (mytarget.catalog == null) {
				
			}

			if (mytarget.isLoopEnabled) {
			}

			serializedObject.Update ();

			var prop0 = serializedObject.GetIterator ();

			prop0.NextVisible (true);
			EditorGUILayout.PropertyField (prop0);

			while (prop0.NextVisible (false)) {
				if (prop0.name == "loopedDelay" && !mytarget.isLoopEnabled)
					continue;
				EditorGUILayout.PropertyField (prop0);
			}
			
			serializedObject.ApplyModifiedProperties ();
		}

		#endregion
	}
}
#endif