﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
	public abstract class CatalogSO<T> : ScriptableObject where T : CatalogSOItem
	{
		public T[] items;
		//product id based dictionary. this doesn't serialize well in the unity inspector
		// so we'll just live with a duplicate dataset.
		Dictionary<string, T> hashlist;

		#region ScriptableObject

		protected virtual void OnEnable ()
		{
			CacheItemsToHashList ();
		}

		#endregion

		#region Private Methods

		void CacheItemsToHashList ()
		{
			hashlist = new Dictionary<string, T> ();
			if (items == null)
				return;
			foreach (var i in items)
				hashlist.Add (i.productId, i);
		}

		T _GetItem (string productid)
		{
			T result;
			if (hashlist.TryGetValue (productid, out result))
				return result;
			Debug.LogError ("Failed to find CatalogItem: " + productid + " in Catalog: " + this.name);
			return null;
		}

		string[] _GetProductIds ()
		{
			var result = new List<string> (hashlist.Keys);
			return result.ToArray ();
		}

		#endregion

		#region Public Methods

		public void ProcessItemsToCache ()
		{
			CacheItemsToHashList ();
		}

		public T GetItem (string productid)
		{
			return _GetItem (productid);
		}

		public string[] GetProductIds ()
		{
			return _GetProductIds ();
		}

		#endregion
	}
}